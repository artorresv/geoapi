<kml xmlns="http://www.opengis.net/kml/2.2">
  <Document id="{{layerid}}">
    <name>México</name>
    <open>1</open>
    <Folder>
      <name>{{mdTitle}}</name>
      <open>1</open>
      <Style id="sp.style">
        <LineStyle>
          <color>ff0000ff</color>
          <width>4</width>
        </LineStyle>
        <PolyStyle>
          <fill>0</fill>
        </PolyStyle>
      </Style>
      <Placemark>
        <name>Delimitaci&#x00F3;n geogr&#x00E1;fica</name>
        <description>
          <![CDATA[
            <p style="font-family:sans">{{locationDesc}}</p>
            <p style="font-family:sans">
              Ver consulta nuevamente en el <a href="{{{webLink}}}">geoportal</a>
            </p>
            <hr/>
            <p style="font-family:sans">
              <a href="http://www.conabio.gob.mx/">CONABIO</a> | <a href="http://www.snib.mx/">SNIB</a>. &copy; {{cYear}}
            </p>
          ]]>
        </description>
        <styleUrl>#sp.style</styleUrl>
        {{{geom}}}
      </Placemark>
    </Folder>
  </Document>
</kml>
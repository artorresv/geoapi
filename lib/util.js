'use strict';

const shortid = require('shortid');

const cfg = {
  app: {
    creator: 'Geoportal del Sistema Nacional de Información sobre Biodiversidad (SNIB)',
    publisher: 'Comisión Nacional para el Conocimiento y Uso de la Biodiversidad',
    pdf: {producer: 'PDFMake with PDFKit'},
    months: {'01':'enero','02':'febrero','03':'marzo','04':'abril','05':'mayo','06':'junio','07':'julio','08':'agosto','09':'septiembre','10':'octubre','11':'noviembre','12':'diciembre'},
    url: {
      base: 'http://geoportal.conabio.gob.mx/',
      metadata: 'metadatos/doc/dc.json/',
      confirmation: 'servicios/datos/avisos/',
      downloads: 'descargas/mapas/dinamicos/'
    },
    sharedCacheHeaders: {'Cache-Control': 'public, max-age=3600, s-maxage=15552000'},
    privateCacheHeaders: {'Cache-Control': 'public, max-age=600, must-revalidate'},
    formats: {
      geojson: "{\"type\":\"FeatureCollection\",\"totalFeatures\":null,\"features\":[],\"crs\":{\"type\":\"name\",\"properties\": {\"name\":\"urn:ogc:def:crs:EPSG::3857\"}}}"
    },
    excludedFields: "'area','perimeter','length','the_geom','cov_','cov_id','spid','ispid','geoid'",
    displayFields: "\"grupobio\",\"subgrupobio\",\"familiavalida\",\"generovalido\",\"especievalida\",\"nom059\",\"cites\",\"iucn\",\"prioritaria\",\"exoticainvasora\",\"longitud\",\"latitud\",\"estadomapa\",\"municipiomapa\",\"localidad\",\"fechacolecta\",\"anp\",\"probablelocnodecampo\",\"categoriaresidenciaaves\",\"formadecrecimiento\",\"fuente\",\"taxonextinto\",\"usvserieVI\",\"urlejemplar\",\"idejemplar\",\"ultimafechaactualizacion\"",
    displayNames: {
      bacterias: "snibbactgw",
      invertebrados: "snibinvegw",
      peces: "snibpecegw",
      protoctistas: "snibprotgw",
      reptiles: "snibreptgw",
      plantas: "snibplangw",
      aves: "snibavesgw",
      anfibios: "snibanfigw",
      mamiferos: "snibmamigw",
      hongos: "snibhonggw"
    }
  },
  re: {
    date: /^(\d{4})-(\d{2})-(\d{2})$/,
    query: /punto\((-?\d+(?:\.\d+)),(-?\d+(?:\.\d+))\)/i,
    buffer: /\d{2,4}/,
    startIndex: /\d+/,
    maxFeatures: /(1|20)/
  }
};

module.exports.cfg = cfg;

module.exports.getLocaleDateTime = (isoDate, opt = {lang: 'es', offsetDays: 0}) => {
  
  if (opt.offsetDays){
    isoDate.setDate(isoDate.getDate() + opt.offsetDays);
  }
  
  return {
    date: isoDate.getDate() + ' de ' + cfg.app.months[(isoDate.getMonth() + 1).toString().padStart(2, '0')] + ' de ' + isoDate.getFullYear(),
    time: isoDate.getHours().toString().padStart(2, '0') + ':' + isoDate.getMinutes().toString().padStart(2, '0') + ':' + isoDate.getSeconds().toString().padStart(2, '0') + ' horas (UTC-' + isoDate.getTimezoneOffset()/60 + ')'
  };
};

module.exports.getGeoJson = (rows, layerName, totalFeatures = 1) => {
  const outData = JSON.parse(cfg.app.formats.geojson);
  
  outData.totalFeatures = totalFeatures;
    
  for (let idx = 0; idx < rows.length; idx++){
    const row = rows[idx],
          geom = JSON.parse(row.geojson),
          fid = layerName + '.' + (row.cov_id || row.idejemplar);
          
    delete row.geojson;
    delete row.the_geom;
    delete row.gid;
    
    outData.features.push({
      type: 'Feature',
      id: fid,
      geometry: geom,
      geometry_name: 'the_geom',
      properties: row
    });
  }
  
  return outData;
};

module.exports.validateQueryParams = (params) => {
  const result = {valid: true, geom: {x: 0, y: 0, buffer: 0}};
  
  if (!params.bufer || !params.f){
    result.valid = false;
  }
  
  if (!cfg.re.query.test(params.f)){
    result.valid = false;
  }
  else{
    result.geom.x = RegExp.$1;
    result.geom.y = RegExp.$2;
  }
  
  if (!cfg.re.buffer.test(params.bufer)){
    result.valid = false;
  }
  else{
    const buffer = parseInt(params.bufer, 10);
    
    result.geom.buffer = (buffer < 25)?25:(buffer > 2500)?2500:buffer;
  }
    
  return result;
};

module.exports.validateNavigationParams = params => {
  if (!params.maxFeatures || !params.startIndex || !params.totalFeatures){
    return false;
  }
  
  if (!cfg.re.startIndex.test(params.startIndex) || !cfg.re.maxFeatures.test(params.maxFeatures) || !cfg.re.startIndex.test(params.totalFeatures)){
    return false;
  }
  
  return true;
};
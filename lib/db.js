'use strict';

const {Pool} = require('pg');

const database = {geodb: {}, snibdb: {}};

module.exports.database = database;
 
module.exports.query = (id, queryObjetc) => {
  return database[id].query(queryObjetc);
};

module.exports.initialize = pg => {
  database.snibdb = new Pool({
    user: pg.snib.user,
    host: pg.snib.host,
    database: pg.snib.database,
    password: pg.snib.password,
    port: pg.snib.port,
    idleTimeoutMillis: 30000
  });
  
  database.geodb = new Pool({
    user: pg.geodb.user,
    host: pg.geodb.host,
    database: pg.geodb.database,
    password: pg.geodb.password,
    port: pg.geodb.port,
    idleTimeoutMillis: 30000
  });
  
  // TODO: It is important you add an event listener to the pool to catch errors. Just like other event emitters, if a pool emits an error event and no listeners are added node will emit an uncaught error and potentially exit.
  /* 
  database.snibdb.on('error', (err, client) => {
    console.error('Unexpected error on idle client', err);
    process.exit(-1);
  });
   */
};
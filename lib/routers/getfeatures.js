'use strict';

const util = require('../util.js'),
      express = require('express'),
      db = require('../db.js');

module.exports = () => {
  const router = express.Router();
  
  /* 
    http://geos1.conabio.gob.mx:60901/api/simple/getfeature/clima1mgw/0/records?query=point(-11234484.426784651,2531796.2937037237)&buffer=1500&maxFeatures=20&startIndex=0
    http://geos1.conabio.gob.mx:60901/api/simple/getfeature/plantas/1/records?query=point(-11234484.426784651,2531796.2937037237)&buffer=1500&maxFeatures=20&startIndex=0
   */
  router.get('/simple/getfeature/:layerId(\\w+)/:cluster(0|1)/records/', async (req, res, next) => {
    try{
      const qParams = await util.validateQueryParams(req.query);
      const nParams = await util.validateNavigationParams(req.query);
      
      if (!qParams.valid || !nParams){
        throw {statusCode: 400};
      }
      
      const sqlParams = {queryFields: '', db: 'geodb', displayId: req.params.layerId};
      
      if (req.params.cluster === '1'){
        req.params.layerId = util.cfg.app.displayNames[req.params.layerId];
        sqlParams.queryFields = 'ST_AsGeoJSON(ST_Transform(the_geom, 3857)) as geojson, ' + util.cfg.app.displayFields;
        sqlParams.db = 'snibdb';
      }
      else{
        sqlParams.queryFields = 'ST_AsGeoJSON(ST_SimplifyPreserveTopology(ST_Transform(the_geom, 3857), 100)) as geojson, *';
      }
      
      res.status(200).set(util.cfg.app.privateCacheHeaders).json(
        await util.getGeoJson(
          (await db.query(sqlParams.db, {text: `
            SELECT ${sqlParams.queryFields}
            FROM public.${req.params.layerId}
            WHERE ST_Intersects(the_geom, ST_Transform(ST_Buffer(ST_GeomFromText('POINT(${qParams.geom.x} ${qParams.geom.y})', 3857), ${qParams.geom.buffer}, 5), 4326))
            LIMIT $1 OFFSET $2
          `, values: [req.query.maxFeatures, req.query.startIndex]})).rows, sqlParams.displayId, req.query.totalFeatures
        )
      );
      
    } catch(err){
      next(err);
    }
  });
  
  /**
   * Devuelve un registro en formato {@link GeoJSON}, con la geometría simplificada (con una tolerancia de 100 metros).
   * El resultado de este servicio incluye headers http para caché.
   *
   * @name GeoJson by record id
   * @param {string} layerID: identificador de la capa
   * @param {string} cluster: identificador de la base de datos
   * @param {string} recordId: ID del registro (para todo el acervo 'cov_id', para el snib ejemplar 'idejemplar')
   * @returns {GeoJSON} Registro en formato geojson
   * @example
   *   http://geos1.conabio.gob.mx:60901/api/simple/getfeature/plantas/records/01f7a5cf28baa0767cd945b715a03857.json
   *   http://geos1.conabio.gob.mx:60901/api/simple/getfeature/clima1mgw/records/1030.json
   */
  router.get('/simple/getfeature/:layerId(\\w+)/:cluster(0|1)/records/:recordId(\\w+).json', async (req, res, next) => {
    try {
      const sqlParams = {queryFields: '', idField: 'cov_id', db: 'geodb'};
      
      if (req.params.cluster === '1'){
        req.params.layerId = util.cfg.app.displayNames[req.params.layerId];
        sqlParams.queryFields = 'ST_AsGeoJSON(ST_Transform(the_geom, 3857)) as geojson, ' + util.cfg.app.displayFields;
        sqlParams.idField = 'idejemplar';
        sqlParams.db = 'snibdb';
      }
      else{
        sqlParams.queryFields = 'ST_AsGeoJSON(ST_SimplifyPreserveTopology(ST_Transform(the_geom, 3857), 100)) as geojson, *';
      }
            
      res.status(200).set(util.cfg.app.sharedCacheHeaders).json(
        await util.getGeoJson(
          (await db.query(sqlParams.db, {text: `
            SELECT ${sqlParams.queryFields}
            FROM public.${req.params.layerId}
            WHERE ${sqlParams.idField} = $1
          `, values: [req.params.recordId]})).rows, req.params.layerId
        )
      );
      
    } catch (err){
      next(err);
    }
  });
  
  /* 
    http://geoportal.conabio.gob.mx/servicios/datos/hits?f=punto(-12281368.303909259%2C3197108.9519561846)&bufer=2500&l=plantas&cluster=1
    
    http://geoportal.conabio.gob.mx/servicios/datos/cache/estados/mx/30/anfibios.xml
    http://geoportal.conabio.gob.mx/servicios/datos/cache/municipios/mx/30011/anfibios.xml
    
    http://geoportal.conabio.gob.mx/servicios/datos/hits?f=exp(key,value)&l=plantas&cluster=1
    key: (estado|municipio)
    value: 30
    
    id(estado|municipio)mapa=30
    mt24id(estado|municipio)mapa=30
    
    http://geoportal.conabio.gob.mx/servicios/datos/cache/anp/mx/6101125/anfibios.xml
    
    http://geoportal.conabio.gob.mx/servicios/datos/hits?f=exp(key,value)&l=plantas&cluster=1
    key: anp
    value: 6101125
    
    idanpfederal1=6101125
    idanpfederal2=6101125
    
    http://geoportal.conabio.gob.mx/servicios/datos/cache/cuencas/mx/1462/anfibios.xml
    key: (anp|estados|municipios|cuencas|ecorregiones|regionmarina)
    value: 1462
    
    http://geoportal.conabio.gob.mx/servicios/datos/hits?f=exp(key,value)&l=plantas&cluster=1
    INTERSECTS(the_geom, querySingle('cnb:cuencasmx', 'the_geom', 'cov_id=1462'))

   */
  router.get('/simple/getfeature/:layerId(\\w+)/:cluster(0|1)/hits/', async (req, res, next) => {
    try {
      const qParams = await util.validateQueryParams(req.query);
      
      if (!qParams.valid){
        throw {statusCode: 400};
      }
      
      const sqlExp = {db: 'geodb'};
      
      if (req.params.cluster === '1'){
        req.params.layerId = util.cfg.app.displayNames[req.params.layerId];
        sqlExp.db = 'snibdb';
      }
      
      /* Hits del SNIB para estados y municipios
         
         http://geoportal.conabio.gob.mx/servicios/datos/cache/estados/mx/30/anfibios.xml
         
         RewriteCond "%{REQUEST_URI}" "^/servicios/datos/cache/\w+/mx/\w+/(?:anfibios|aves|bacterias|hongos|invertebrados|mamiferos|peces|plantas|protoctistas|reptiles)\.xml$"
         RewriteCond "%{REQUEST_URI}" "^.+/(municipio|estado)s/mx/(\w+)/(\w+).+$"
         RewriteRule ^.+$ 'http://172.16.1.235:8080/geoserver/wfs?srsName=EPSG:4326&request=GetFeature&version=1.1.0&outputFormat=application/json&ResultType=hits&typeName=%3&propertyNames=&cql_filter=id%1mapa=%2+OR+mt24id%1mapa=%2' [P,NE,QSD]

         cql_filter=id(estado|municipio)mapa=30+OR+mt24id(estado|municipio)mapa=30
        
         Hits del SNIB para AANNPP
        
         http://geoportal.conabio.gob.mx/servicios/datos/cache/anp/mx/6101125/anfibios.xml
         
         RewriteCond "%{REQUEST_URI}" "^/servicios/datos/cache/\w+/mx/\w+/(?:anfibios|aves|bacterias|hongos|invertebrados|mamiferos|peces|plantas|protoctistas|reptiles)\.xml$"
         RewriteCond "%{REQUEST_URI}" "^.+/(?:anp)/mx/(\w+)/(\w+).+$"
         RewriteRule ^.+$ 'http://172.16.1.235:8080/geoserver/wfs?srsName=EPSG:4326&request=GetFeature&version=1.1.0&outputFormat=application/json&ResultType=hits&typeName=%2&propertyNames=&cql_filter=idanpfederal1=%1+OR+idanpfederal2=%1' [P,NE,QSD]

         Todo el acervo y el SNIB con cuencas, ecorregiones y reg. marinas
         
         http://geoportal.conabio.gob.mx/servicios/datos/cache/cuencas/mx/1462/anfibios.xml
         
         RewriteRule ^/servicios/datos/cache/(anp|estados|municipios|cuencas|ecorregiones|regionmarina)/mx/(\w+)/(\w+)\.xml$ http://172.16.1.235:8080/geoserver/wfs?srsName=EPSG:4326&request=GetFeature&version=1.1.0&outputFormat=application/json&ResultType=hits&typeName=$3&propertyNames=&cql_filter=INTERSECTS(the_geom,querySingle(\'cnb:$1mx\',\'the_geom\',\'cov_id=$2\')) [P,NE,QSD]
       */
       
      res.status(200).json((await db.query(sqlExp.db, `
        SELECT COUNT(*)
        FROM public.${req.params.layerId}
        WHERE ST_Intersects(the_geom, ST_Transform(ST_Buffer(ST_GeomFromText('POINT(${qParams.geom.x} ${qParams.geom.y})', 3857), ${qParams.geom.buffer}, 5), 4326))
      `)).rows[0]);
      
    } catch (err) {
      next(err);
    }
    
  });
  
  return router;
};
'use strict';

const util = require('../util.js'),
      express = require('express'),
      shortid = require('shortid'),
      archiver = require('archiver'),
      talisman = require('talismanjs'),
      stringify = require('csv-stringify'),
      QueryStream = require('pg-query-stream'),
      db = require('../db.js'),
      fs = require('fs'),
      rp = require('request-promise'),
      buffer = require('@turf/buffer'),
      {point} = require('@turf/helpers');

module.exports = () => {
  const router = express.Router();
  
  /*
   * http://172.16.1.84:60901/api/simple/getfeature/plantas/download?l=plantas&f=punto(-99.1820,19.3185)&bufer=1500
   *
   * http://geoportal.conabio.gob.mx/servicios/datos/descarga?l=plantas&f=punto(-99.1820,19.3185)&bufer=1500&t=25072018
   */
  router.get('/simple/getfeature/:layerId(\\w+)/download/', async (req, res, next) => {
    let client = null;
    try {
      const qParams = await util.validateQueryParams(req.query);
      
      if (!qParams.valid){
        throw {statusCode: 400};
      }
      
      const sqlParams = {headers: {}, fields: '', db: 'geodb', cluster: false},
            md = (await rp({url: util.cfg.app.url.base + util.cfg.app.url.metadata + req.params.layerId + '.json', gzip: true, json:true}));
      
      if (md.keywords && md.keywords.cluster === 'true'){
        req.params.layerId = util.cfg.app.displayNames[req.params.layerId];
        sqlParams.db = 'snibdb';
        sqlParams.cluster = true;
      }
            
      client = await db.database[sqlParams.db].connect();
      
      const tblHeaders = (await client.query(`
        SELECT string_agg(${sqlParams.cluster ? "concat('\"', column_name, '\"')" : "column_name"}, ',')
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = '${req.params.layerId}' AND
        column_name NOT IN (${util.cfg.app.excludedFields})
      `)).rows[0].string_agg;
            
      sqlParams.fields = tblHeaders;
      
      if (sqlParams.cluster){
        sqlParams.fields = sqlParams.fields.replace('"ultimafechaactualizacion"', 'to_char(ultimafechaactualizacion, \'YYYY-MM-DD\') as ultimafechaactualizacion');
        sqlParams.headers = tblHeaders.replace(/\"/g,'').split(',');
      }
      else{
        sqlParams.fields += ',ST_AsText(the_geom) AS geometria';
        sqlParams.headers = tblHeaders.split(',');
        sqlParams.headers.push('geometria');
      }
      
      const dateCmp = util.cfg.re.date.exec(md.ISOLastModified || md.lastModified).splice(1),
            maxZipEntries = 2,
            uuid = shortid.generate(),
            fNames = {
                       zip: md.id + '.' + dateCmp.join('.') + '.' + uuid + '.csv.zip',
                       csv: md.id + '.csv',
                       // pdf: md.id + '.pdf',
                       kml: 'limites.kml',
                       txt: 'importante.txt'
                     },
            zip = fs.createWriteStream(req.app.locals.static + fNames.zip).on('error', err => next(err)),
            archive = archiver('zip', {zlib: {level: 9}}).on('error', err => next(err)).on('warning', err => next(err));
            
      zip.on('close', () => {
        if (archive._state.aborted){
          next({statusCode: 500});
        }
        else{
          res.location(util.cfg.app.url.base + util.cfg.app.url.downloads + fNames.zip).sendStatus(303);
        }
      });
      
      archive.on('entry', fileInfo => {
        if (fileInfo.type === 'file'){
          fs.unlink(fileInfo.sourcePath, err => {if (err) next(err);});
        }
        
        if (archive._entriesProcessedCount === maxZipEntries){
          archive.finalize();
        }
      }).pipe(zip);
      
      const sPolygon = await buffer(point([qParams.geom.x, qParams.geom.y]), qParams.geom.buffer, {units: 'meters', steps: 5}),
            intersectGeom = JSON.stringify(sPolygon.geometry),
            query = new QueryStream(`
              SELECT ${sqlParams.fields}
              FROM public.${req.params.layerId}
              WHERE ST_Intersects(the_geom, ST_SetSRID(ST_GeomFromGeoJSON('${intersectGeom}'), 4326))
            `, [], {batchSize:250}),
            txtTpl = await talisman.create('./views/importante.tpl'),
            kmlTpl = await talisman.create('./views/kml.tpl'),
            locDate = new Date(),
            formatedDate = locDate.getDate() + ' de ' + util.cfg.app.months[(locDate.getMonth() + 1).toString().padStart(2, '0')] + ' de ' + locDate.getFullYear(),
            formatedTime = locDate.getHours().toString().padStart(2, '0') + ':' + locDate.getMinutes().toString().padStart(2, '0') + ':' + locDate.getSeconds().toString().padStart(2, '0') + ' horas (UTC-' + locDate.getTimezoneOffset()/60 + ')',
            htmlPermLink = `${util.cfg.app.url.base}metadatos/doc/html/${md.id}.html`,
            dataPermLink = `${util.cfg.app.url.base}servicios/datos/descarga?l=${md.id}&f=punto(${qParams.geom.x},${qParams.geom.y})&bufer=${qParams.geom.buffer}&t=${dateCmp[2] + dateCmp[1] + dateCmp[0]}`,
            webPermLink = `${util.cfg.app.url.base}#!l=${md.id}:1@f=punto(${qParams.geom.x},${qParams.geom.y}).bufer(${qParams.geom.buffer})@t=${dateCmp[2] + dateCmp[1] + dateCmp[0]}`,
            mdSubject = `ubicados en un radio de ${(qParams.geom.buffer/1000).toLocaleString()} km. respecto a la coordenada: ${qParams.geom.x} (longitud), ${qParams.geom.y} (latitud)`,
            queryMD = (await client.query(`
              SELECT COUNT(*) AS count, ST_AsKML(g.geom) AS kml, ST_AsGeoJSON(ST_Envelope(g.geom), 15, 1) AS bbox
              FROM ${req.params.layerId}, (
                SELECT ST_SetSRID(ST_GeomFromGeoJSON('${intersectGeom}'),4326) AS geom
              ) AS g
              WHERE ST_Intersects(the_geom, g.geom)
              GROUP BY g.geom
            `)).rows[0];
            
            /* 
             * Por ahora omito metadato en pdf...
             */
            /* const queryBbox = JSON.parse(queryMD.bbox).bbox; */
                        
      await client.query(query)
            .on('error', err => next(err))
            .pipe(stringify({quoted:true, columns:sqlParams.headers, header: true}))
            .pipe(fs.createWriteStream(req.app.locals.static + uuid + '.' + fNames.csv)
            .on('error', err => next(err)))
            .on('close',() => archive.file((req.app.locals.static + uuid + '.' + fNames.csv), {name: fNames.csv}));

      await kmlTpl.set({geom: queryMD.kml, mdTitle: md.title, layerid: md.id, webLink: webPermLink, cYear: locDate.getFullYear(), locationDesc: `Radio de ${(qParams.geom.buffer/1000).toLocaleString()} km. respecto a la coordenada: ${qParams.geom.x} (longitud), ${qParams.geom.y} (latitud)`}).toStream()
            .on('error', err => next(err))
            .pipe(fs.createWriteStream(req.app.locals.static + uuid + '.' + fNames.kml))
            .on('error', err => next(err))
            .on('close', () => archive.file((req.app.locals.static + uuid + '.' + fNames.kml), {name: fNames.kml}));
      
      await txtTpl.set({mdCreator: util.cfg.app.creator, htmlLink: htmlPermLink, dataLink: dataPermLink, webLink: webPermLink, recordCount: queryMD.count, suuid: uuid, mdTitle: md.title, shortLastMod: dateCmp.join('.'), padLayerid: md.id.padEnd(13), layerid: md.id, locDate: formatedDate, locTime: formatedTime, mdTitleDesc: mdSubject, locLastMod: dateCmp[2] + ' de ' + util.cfg.app.months[dateCmp[1]] + ' de ' + dateCmp[0]}).toStream()
            .on('error', err => next(err))
            .pipe(fs.createWriteStream(req.app.locals.static + uuid + '.' + fNames.txt))
            .on('error', err => next(err))
            .on('close',() => archive.file((req.app.locals.static + uuid + '.' + fNames.txt), {name: fNames.txt}));
      
      /* 
       * Por ahora omito metadato en pdf...
       */
      /* const pdfKitDoc = await req.app.locals.printer.createPdfKitDocument({
              pageSize: 'LETTER',
              pageMargins: [40, 60, 40, 40],
              footer: (p, c) => {
                return {text: (p.toString() + ' de ' + c), style: ['red', 'footer', 'small', 'right']};
              },
              header:{
                layout: 'headerLayout',
                table: {
                  headerRows: 1,
                  widths: ['*', 'auto'],
                  body: [
                    [
                      [
                        {text: util.cfg.app.publisher, style: ['red', 'header1', 'small', 'bold']},
                        {text: util.cfg.app.creator, style: ['red', 'header2', 'small']}
                      ],
                      {text: formatedDate, style: ['red', 'header3', 'small', 'right']}
                    ]                    
                  ]
                }
              },
              info: {
                title: md.title,
                author: md.author[0].name,
                subject: 'Registros ' + mdSubject,
                keywords: '',
                creator: util.cfg.app.creator,
                modDate: locDate,
                producer: util.cfg.app.pdf.producer
              },
              styles: {
                red: {color: '#943634'},
                blue: {color: '#0000ff'},
                bold: {bold: true},
                italic: {italics: true},
                justified: {alignment: 'justify'},
                right: {alignment: 'right'},
                small: {fontSize: 9},
                medium: {fontSize: 10},
                header1: {margin: [40,30,0,0]},
                header2: {margin: [40,0,0,0]},
                header3: {margin: [0,30,40,0]},
                footer: {margin: [0,0,40,0]},
                subtitle: {margin: [0,10,0,10]},
                simpleTable: {alignment: 'justify', fontSize: 10}
              },
              content: [
                {text: 'INFORMACIÓN BÁSICA', style: ['subtitle', 'medium', 'bold']},
                {
                  ul: [
                    [
                      {text: 'Título: ', style: ['bold', 'medium']},
                      {text: ('Consulta de registros del mapa "' + md.title + '", ' + mdSubject), style: ['medium', 'justified']}
                    ],
                    [
                      {text: 'Autor(es): ', style: ['bold', 'medium']},
                      {text: md.author[0].name, style: ['medium', 'justified']}
                    ],
                    [
                      {text: 'Descripción: ', style: ['bold', 'medium']},
                      [
                        {text: md.description, style: ['medium', 'justified']},
                        {text: 'ver más', style: ['medium', 'blue'], link: htmlPermLink}
                      ]
                    ],
                    [
                      {text: 'Última actualización de los datos: ', style: ['bold', 'medium']},
                      {text: dateCmp[2] + ' de ' + util.cfg.app.months[dateCmp[1]] + ' de ' + dateCmp[0], style: ['medium', 'justified']}
                    ],
                    [
                      {text: 'Fecha de consulta: ', style: ['bold', 'medium']},
                      {text: formatedDate + ' a las ' + formatedTime, style: ['medium', 'justified']}
                    ],
                    [
                      {text: 'Accesos directos: ', style: ['bold', 'medium']},
                      {ul: [
                        [
                          {text: 'Descargar los datos nuevamente: ', style: ['bold', 'medium']},
                          {text: dataPermLink, style: ['medium', 'blue'], link: dataPermLink}
                        ],
                        [
                          {text: 'Repetir la consulta desde el geoportal: ', style: ['bold', 'medium']},
                          {text: webPermLink, style: ['medium', 'blue'], link: webPermLink}
                        ],
                        [
                          {text: 'Todos los registros disponibles del mapa "' + md.title + '": ', style: ['bold', 'medium']},
                          {text: htmlPermLink, style: ['medium', 'blue'], link: htmlPermLink}
                        ]
                      ]}
                    ],
                    [
                      {text: 'Límites geográficos: ', style: ['bold', 'medium']},
                      {
                        style: 'simpleTable',
                        layout: 'noBorders',
                        table: {
                          headerRows: 0,
                          widths: ['auto', '*'],
                          body: [
                            [{text: 'Oeste:'}, {text: queryBbox[0]}],
                            [{text: 'Este:'}, {text: queryBbox[2]}],
                            [{text: 'Norte:'}, {text: queryBbox[3]}],
                            [{text: 'Sur:'}, {text: queryBbox[1]}]
                          ]
                        }
                      }
                    ],
                    [
                      {text: 'Forma sugerida de citar: ', style: ['bold', 'medium']},
                      {text: 'CONABIO. 2018. Sistema Nacional de Información sobre Biodiversidad. Registros de ejemplares de peces. Comisión Nacional para el Conocimiento y Uso de la Biodiversidad. Consulta de 98 proyectos realizada el 2018-07-25 http://geoportal.conabio.gob.mx/acceso/peces/2018/07/25 Ciudad de México, México.', style: ['medium', 'italic', 'justified']}
                    ],
                    [
                      {text: 'Condiciones de uso: ', style: ['bold', 'medium']},
                      {text: 'Si usted utiliza la información disponible en este mapa se compromete a respetar las condiciones de uso específicas de cada fuente.', style: ['medium', 'justified']}
                    ]
                  ]
                },
                {text: 'PROCESAMIENTO', style: ['subtitle', 'medium', 'bold']},
                {text: 'INFORMACIÓN GEOGRÁFICA', style: ['subtitle', 'medium', 'bold']},
                {text: 'FUENTES', style: ['subtitle', 'medium', 'bold']},
                {text: 'CAMPOS', style: ['subtitle', 'medium', 'bold']}
              ]
            },
            {
              tableLayouts: {
                headerLayout: {
                  hLineWidth: function (i, node) {
                    return (i === node.table.headerRows) ? 1 : 0;
                  },
                  vLineWidth: function (i) {
                    return 0;
                  },
                  hLineColor: function (i) {
                    return '#943634';
                  },
                  paddingLeft: function (i) {
                    return 0;
                  },
                  paddingRight: function (i, node) {
                    return 0;
                  }
                }
              }
            }
            ); */
      
      /* 
       * TODO:
       * One important caveat is that if the Readable stream emits an error during processing, the Writable destination is not closed automatically. If an error occurs, it will be necessary to manually close each stream in order to prevent memory leaks.
       * https://nodejs.org/docs/latest-v8.x/api/stream.html#stream_readable_pipe_destination_options
       */
      
      /* 
       * Por ahora omito metadato en pdf...
       */
      /* pdfKitDoc
        .on('error', err => next(err))
        .pipe(fs.createWriteStream(req.app.locals.static + uuid + '.' + fNames.pdf))
        .on('error', err => next(err))
        .on('close',() => {
          if (archive.writable){
            archive.file((req.app.locals.static + uuid + '.' + fNames.pdf), {name: fNames.pdf});
          }
        });
        
      await pdfKitDoc.end(); */
            
    } catch (err) {
      next(err);
    } finally {
      if (client !== null) {
        client.release();
      }
    }
  });
    
  return router;
};
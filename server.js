'use strict';

const pkg = require('./package.json'),
      errorHandler = require('./lib/handlers.js'),
      PdfPrinter = require('pdfmake'),
      {URL} = require('url'),
      path = require('path'),
      nconf = require('nconf');
      
nconf.argv().env('__').defaults({'NODE_ENV': 'development'});

const NODE_ENV = nconf.get('NODE_ENV');
const isDev = NODE_ENV === 'development';

nconf
  .defaults({'conf': path.join(__dirname, `${NODE_ENV}.config.json`)})
  .file(nconf.get('conf'));

const serviceUrl = new URL(nconf.get('serviceUrl'));
const servicePort = serviceUrl.port || (serviceUrl.protocol === 'https:' ? 443 : 80);

const express = require('express'),
      db = require('./lib/db.js').initialize(nconf.get('pg'));

const app = express();

/* Config settings */
if (isDev){
  app.use(require('morgan')('dev'));  
}

app.use(express.static('public/static', {etag:false, lastModified:true}))
app.set('etag', false);
app.set('x-powered-by', false);

/* Global app objects */
app.locals.static = nconf.get('staticDir');
app.locals.printer = new PdfPrinter({Roboto: nconf.get('fonts')});
app.locals.isDev = isDev;

/* Operational routers */
app.get('/api/simple/version', (req, res) => res.status(200).json(pkg.version));
app.use('/api', require('./lib/routers/getfeatures.js')());
app.use('/api', require('./lib/routers/downloads.js')());

/* Error handling routers */
app.use(errorHandler.applicationLog);
app.use(errorHandler.clientErrorHandler);
app.use(errorHandler.defaultErrorHandler);

app.listen(servicePort, () => console.log('Ready...'));

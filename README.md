**REQUERIMIENTOS**

  * Ubuntu > 18.x
  * Nodejs > 8.x
  * npm > 6.x (creo que se instala por omisión con nodejs)

**INSTALACION**

1. Asumiendo que este directorio existe y se tienen permisos de escritura:    

    `cd /opt/webserver`

2. Clonar el repositorio:

    `git clone https://artorresv@bitbucket.org/artorresv/geoapi.git`
    
3. El procedimiento anterior crea un directorio con el código fuente:

    `cd geoapi`

4. Instalar las dependencias de nodejs:

    `NODE_ENV=production npm install`

5. Después de instaladas, copiar los siguientes archivos (revisar el contenido para ver qué puerto usa, etc.) según la instalación sea para desarrollo o para producción:

    Producción: `scp geos1.conabio.gob.mx:/home/atorres/bin/geoportal/servicios/geoapi/production.config.json .`

    Desarrollo: `scp geos1.conabio.gob.mx:/home/atorres/bin/geoportal/servicios/geoapi/development.config.json .`

**USO**

1. Incio normal de la aplicación:
    
    a. Con npm:
    
       `NODE_ENV=production npm run start`
       
    b. Con pm2 (ver ecosystem.config.json para más detalles):
    
       `pm2 start ecosystem.config.json --env production`
      
2. Incio para [depuración](https://nodejs.org/en/docs/guides/debugging-getting-started/) con npm:

    `NODE_ENV=production npm run start:debug`
    
3. La idea que tengo es dividir la aplicación en dos grupos de comandos:

     1. **Simples**:
     
        Para geometrías simples * (puntos con radio, líneas con radio, etc.) y predefinidas (por AANNPP, municipios, etc.), sin sesión, es decir, que no dependen del estado de la aplicación (el geoportal o cualquier otra)
        
     2. **Completos**:
     
        Para cualquier tipo de geometría, sin restricciones de área o vértices. En el geoportal dependerían de una sesión activa (cookies)      
        
        \* aquí geometrías "simples" significa: muy pocos vértices, que se pueden codificar en un url, e.g. query=line(-99,19 -99,18)&buffer=2000

4. Operaciones:

      Para los dos grupos inicialmente estarían disponibles las sig. operaciones (todas usan intersección espacial):
      
      1. Cuenta de registros:
      
         http://geoportal.conabio.gob.mx/servicios/datos/hits?f=punto(-12230924.685240917%2C3213442.6005383614)&bufer=2500&l=anfibios&cluster=1
       
      2. Páginado, de 1 y 20 registros:
      
        http://geoportal.conabio.gob.mx/servicios/datos/records?l=aves&cluster=1&bufer=2500&totalFeatures=2&f=punto(-12233838.848593052%2C3212353.3733193325)&startIndex=1&maxFeatures=1
         
      3. Descarga de los datos (csv), con metadato (pdf), geometría usada para filtrar (*.kml) y notas de la descarga (*.txt), todos compactados en zip:
      
         http://geoportal.conabio.gob.mx/servicios/datos/descarga?l=aves&f=punto(-109.94466403773771,27.612678172734515)&bufer=2500
         
5. Comentarios:
     * Ese código es el mismo que se usará para la versión completa
     * Ambos grupos de comandos se podrán usar como servicios independientes de la página del geoportal
     * Están disponibles para todas las capas vectoriales del acervo (~7,000)
     
**PENDIENTES**

1. Para una versión operativa de la versión simple (para uso en el geoportal) y la descarga de datos para la versión completa

    * limpieza general del código
    * servicio de limpieza de archivos auxiliares, como cron del SO
